% Accueil - Mini-DebConf 2020 - Bordeaux

## Annonce

En raison du coronavirus (COVID-19), l'évènement initialement prévu les 10 et 11 Octobre 2020 est reporté à 2021.


## Liens utiles

+ Wiki Debian [DebianEvents/fr/2020/Bordeaux](https://wiki.debian.org/DebianEvents/fr/2020/Bordeaux)
+ Vous souhaitez contributer au site ? [Voir les sources sur Salsa](https://salsa.debian.org/debian-france-team/mini-debconf-fr/2020-bordeaux/website)
