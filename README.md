Site de la Mini-DebConf 2020 de Bordeaux
========================================

----------------------------------------

Page publique du site : [https://mini-debconf-fr.debian.net/2020-bordeaux](https://mini-debconf-fr.debian.net/2020-bordeaux)

----------------------------------------

## Comment contribuer ?

+ Le style est éditable dans le fichier [pages/style.css](pages/style.css)
+ La page d'accueil est éditable dans le fichier [src/index.md](src/index.md)
+ Le haut du site (menu) est éditable dans le fichier [src/01-top.template](src/01-top.template)
+ Le bas du site (pied de page) est éditable dans le fichier [src/02-bottum.template](src/02-bottum.template)
